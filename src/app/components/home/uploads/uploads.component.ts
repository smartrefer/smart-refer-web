import * as moment from 'moment-timezone';
import { Component, OnInit,Input, ViewChild} from '@angular/core';
import { Router,NavigationExtras } from '@angular/router';
import {slideInOutAnimation,fadeInAnimation} from '../../../animations/index';
import { Table } from 'primeng/table';

import { KetAttachmentTypeService } from '../../../services-api/ket-attachment-type.service';
import { KetAttachmentService } from '../../../services-api/ket-attachment.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ServicesService } from '../../../services-api/services.service';
import { AlertService } from '../../../service/alert.service';

@Component({
  selector: 'app-uploads',
  templateUrl: './uploads.component.html',
  styleUrls: ['./uploads.component.css']
})
export class UploadsComponent implements OnInit {

    scrHeight: number = 0;
    scrWidth: number = 0;
    boxHeight: any;
    rowsData: any[] = [];
    device:any = 't';
    
    // today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
    today: Date = new Date();
  
    blockedPanel: boolean = false;
    refer: any = {};
  
    // customers: Customer[] = [];
  
    // selectedCustomers: Customer[] = [];
  
    // representatives: Representative[] = [];
    selectedRowsData:any = [];
  
    file: File | null = null
    uploadfile:any;
    att_type:any;
    att_name:any;

    statuses: any[] = [];
  
    loading: boolean = true;
  
    displayCreate: boolean = false;
    rowsDataCreate: any[] = [];
    loadingCreate: boolean = true;
  
    displayReferBack: boolean = false;
    rowsDataReferBack: any[] = [];
    loadingReferBack: boolean = true;
  
  
    
  
    @ViewChild('dt') table!: Table;
  
    @ViewChild('dtCreate') tableCreate!: Table;
  
    first: any = 0;
  
    rows: any = 10;
  
    hcode: any;
  
    refer_no: string ='';
    sdateCreate: any;
    edateCreate: any;
  
    limitReferBack: any = 2000;
    fullname: string ='';
    code: any;
    description: any;
    itemeTypeAtt: any = [];

    buttonClassStrength: string = 'p-button-rounded p-button-warning';
    value3: any;
    justifyOptions: any = [];
    value1: string = "off";
    stateOptions: any = [];
  
    ptableStyle: any = {
      width: '100%',
      height: '100%',
      flex: '1 1 auto'
    };
    scrollHeight: string = '';
    validateForm:boolean = false;

  constructor(
    private ketAttachmentService: KetAttachmentService,

    private ketAttachmentTypeService: KetAttachmentTypeService,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private alertService: AlertService,
  ) {
    this.hcode = sessionStorage.getItem('hcode');
   }

  ngOnInit(): void {
    this.getInfo();
    this.getTypeAtt();
  }

  onFileInput(files: FileList | null): void {
    if (files) {
      this.file = files.item(0)
    }
  }

  onSaveUpload(){
    if(this.code){
      this.onUpload();
    }else{
      this.alertService.error('กรุณาเลือกข้อมูล ประเภทเอกสาร');
    }
  }

  async onUpload() {
    // `${this.refer.provider_code}-1-${this.refer.referno}`
    let refer_no:any = `${this.refer_no}`
    let info:any = {
      "att_type":`${this.code}`,
      "hcode":`${this.hcode}`
    }
    
    if (this.file) { 
      let subscription:any = await this.ketAttachmentService.upload(this.file,refer_no,info);
      this.uploadfile = subscription.file.path;
      this.getInfo();
      this.code ="";
      this.file = null;
    }
    
  }
  async getTypeAtt() {
    try {
      let rs: any = await this.ketAttachmentTypeService.select();
      this.itemeTypeAtt = rs;
    } catch (error) {
      console.log('error',error);
    }
  }

  async getInfo() {
    let items: any=sessionStorage.getItem('strdata');
    let i:any =JSON.parse(items);

    this.loading = true;
    this.refer_no = i.refer_no;
    this.fullname = i.fullname;

    try {
      let rs: any = await this.ketAttachmentService.select(i.refer_no);

      let item: any = rs[0];

      if (item) {

        this.rowsData = rs;
        this.loading = false;
      } else {
        this.loading = false;
      }
    } catch (error) {
      console.log(error);
      this.loading = false;
    }
    
  }
  
  async downloadAttRoute(filename: any){
    await this.ketAttachmentService.download(filename);
  
  }

  async deleteAttRoute(id : any){
    let confirm:any = await this.alertService.confirm("ต้องการลบรายการนี้ ใช่หรือไม่");

    if(confirm){
      try{
        let del: any = await this.ketAttachmentService.delete(id);

        this.getInfo();
      }catch(error){

      };
    }else{

    }


  }
  onDateSelected(){
    // console.log('lddl');
    // if(this.edate < this.sdate){     
    //   this.alertService.error("ข้อผิดพลาด !", "วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้");
    //   this.validateForm = false;
    // }else{
    //   this.validateForm = true;
    // }
  }

  readGlobalValue() {
    this.scrHeight = Number(this.globalVariablesService.scrHeight);
    this.scrWidth = Number(this.globalVariablesService.scrWidth);
    this.boxHeight = ((this.scrHeight) - 80) + 'px';
    this.scrollHeight = (Number(this.globalVariablesService.scrHeight) - 400) + 'px';
    this.ptableStyle = {
      width: (this.scrWidth-70) + 'px',
      
      height: (this.scrHeight - 300) + 'px'
    }
    // do something with the value read out
  }

  onRowSelect(event:any){
    // console.log(event);
    // console.log(data);
  }

}
