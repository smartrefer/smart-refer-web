import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import * as moment from 'moment-timezone';
import { Table } from 'primeng/table';
// import { PrimeNGConfig } from 'primeng/api';
import { GlobalVariablesService } from '../../../shared/globalVariables.service';
import { slideInOutAnimation } from '../../../animations/index';
import { AlertService } from '../../../service/alert.service';
import { KetReferoutService } from '../../../services-api/ket-referout.service';
import { Router } from '@angular/router';
import { KetAttachmentService } from '../../../services-api/ket-attachment.service';

import { ServicesService } from '../../../services-api/services.service';
import {
    ConfirmationService,
    MessageService,
    ConfirmEventType,
} from 'primeng/api';
import { Message } from 'primeng/api';
import { ClipboardService } from 'ngx-clipboard';

import { PrimeNGConfig } from 'primeng/api';

import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

import { MqttClient } from 'mqtt';
import * as mqttClient from '../../../vendor/mqtt';
import { log } from 'console';

@Component({
    selector: 'app-referin',
    templateUrl: './referin.component.html',
    styleUrls: ['./referin.component.css'],
    animations: [slideInOutAnimation],
    // attach the fade in animation to the host (root) element of this component
    host: { '[@slideInOutAnimation]': '' },
})
export class ReferinComponent implements OnInit {
    @ViewChild('dt') table!: Table;

    @ViewChild('dtCreate') tableCreate!: Table;

    isOffline = false;
    client: MqttClient;
    notifyUser = null;
    notifyPassword = null;
    notifyUrl: string;

    start_in_date: any;
    end_in_date: any;

    itemStorage: any = [];
    itemeStation: any = [];
    spclty_name: any;

    screenStyle: any = {
        width: '100%',
        height: '100%',
        flex: '1 1 auto',
    };
    scrHeight: number = 0;
    scrWidth: number = 0;
    boxHeight: any;
    rowsData: any[] = [];
    rowsData1: any = [];
    rowsData2: any = [];
    device: any = 't';
    scrollableCols: any = [];

    unlockedCustomers: any = [];

    lockedCustomers: any = [];

    balanceFrozen: boolean = false;

    rowGroupMetadata: any;

    checked: boolean = false;

    //today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
    today: Date = new Date();

    blockedPanel: boolean = false;

    // customers: Customer[] = [];

    // selectedCustomers: Customer[] = [];

    // representatives: Representative[] = [];
    selectedRowsData: any = [];

    statuses: any[] = [];

    loading: boolean = true;

    displayCreate: boolean = false;
    rowsDataCreate: any[] = [];
    loadingCreate: boolean = true;

    rowsDataTemp: any[] = [];
    rowsReportData: any = {};

    displayReferOut: boolean = false;
    rowsDataReferOut: any[] = [];
    loadingReferOut: boolean = true;

    first: any = 0;

    rows: any = 10;

    hcode: any;

    sdate: Date;
    edate: Date;

    sdateCreate: any;
    edateCreate: any;

    limitReferOut: any = 2000;

    buttonClassStrength: string = 'p-button-rounded p-button-warning';
    value3: any;
    justifyOptions: any = [];
    value1: string = 'off';
    stateOptions: any = [];
    textHN: any;

    ptableStyle: any = {
        width: '100%',
        height: '100%',
        flex: '1 1 auto',
    };
    scrollHeight: string = '';
    validateForm: boolean = true;
    checked1: boolean = false;
    strChecked1: string = '';

    displayDetail: boolean = false;
    msgs: Message[] = [];
    content: any;
    cid: any;

    exportColumns: any = [];
    paginator: boolean = true;

    constructor(
        private primengConfig: PrimeNGConfig,
        private globalVariablesService: GlobalVariablesService,
        private alertService: AlertService,
        private ketReferoutService: KetReferoutService,
        private router: Router,
        private ketAttachmentService: KetAttachmentService,
        private servicesService: ServicesService,
        private confirmationService: ConfirmationService,
        private clipboardService: ClipboardService,
        private clipboardApi: ClipboardService,
        private messageService: MessageService,
        private zone: NgZone
    ) {
        this.hcode = sessionStorage.getItem('hcode');
        let Station: any = localStorage.getItem('itemeStation');
        this.itemeStation = JSON.parse(Station);

        this.sdate = this.today;
        this.edate = this.today;

        this.sdateCreate = this.today;
        this.edateCreate = this.today;
        this.cid = this.cid;

        // config mqtt
        this.notifyUrl = `ws://203.113.117.66:8080`;
        this.notifyUser = `q4u`;
        this.notifyPassword = `##q4u##`;
    }

    ngOnInit() {
        this.loading = false;
        this.readGlobalValue();
        if (
            sessionStorage.getItem('start_in_date') &&
            sessionStorage.getItem('end_in_date')
        ) {
            this.sdate = JSON.parse(sessionStorage.getItem('start_in_date'));
            this.edate = JSON.parse(sessionStorage.getItem('end_in_date'));
        }

        this.getInfo();
        this.primengConfig.ripple = true;
        this.connectWebSocket();

    }


    activeMenu() {
        // console.log('activeMenu');
        let menuitem = document.querySelectorAll(
            'ul.p-menubar-root-list > li.p-menuitem'
        );
        for (let i = 0; i < menuitem.length; i++) {
            // menuitem[i].classList.remove('active');
            // menuitem[i].classList.remove('activeMe');

            // console.log(i);
        }
        menuitem[1].classList.add('activeMe');
        // console.log(menuitem);
    }

    readGlobalValue() {
        this.scrHeight = Number(this.globalVariablesService.scrHeight);
        this.scrWidth = Number(this.globalVariablesService.scrWidth);
        this.boxHeight = this.scrHeight - 80 + 'px';
        this.scrollHeight =
            Number(this.globalVariablesService.scrHeight) - 360 + 'px';

        this.screenStyle = {
            width: this.scrWidth - 50 + 'px',
            height: this.scrHeight - 200 + 'px',
        };
        this.ptableStyle = {
            width: this.scrWidth - 100 + 'px',

            height: this.scrHeight - 300 + 'px',
        };
        // do something with the value read out
    }

    async getReferIn(i: any) {

        try {
            let rs: any = await this.ketReferoutService.receive(i.refer_no);

            if (rs) {
                // this.diagnosis = rs.diagnosis
            } else {
            }
        } catch (error) {
            console.log(error);
        }
    }

    toggleLock(data: any, frozen: any, index: any) {
        if (frozen) {
            this.lockedCustomers = this.lockedCustomers.filter(
                (c: any, i: any) => i !== index
            );
            this.unlockedCustomers.push(data);
        } else {
            this.unlockedCustomers = this.unlockedCustomers.filter(
                (c: any, i: any) => i !== index
            );
            this.lockedCustomers.push(data);
        }

        this.unlockedCustomers.sort((val1: any, val2: any) => {
            return val1.id < val2.id ? -1 : 1;
        });
    }

    formatCurrency(value: any) {
        return value.toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD',
        });
    }

    onDateSelected() {
        if (this.edate < this.sdate) {
            this.alertService.error(
                'ข้อผิดพลาด !',
                'วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้'
            );
            this.validateForm = false;
        } else {
            this.validateForm = true;
        }
    }

    async getInfo() {
        this.loading = true;
        this.rowsData = [];
        this.rowsData1 = [];
        this.rowsData2 = [];

        let startDate: any;
        let endDate: any;
        this.start_in_date = moment(
            JSON.parse(sessionStorage.getItem('start_in_date'))
        ).format('YYYY-MM-DD');
        this.end_in_date = moment(
            JSON.parse(sessionStorage.getItem('end_in_date'))
        ).format('YYYY-MM-DD');


        if (
            sessionStorage.getItem('start_in_date') &&
            sessionStorage.getItem('end_in_date')
        ) {
            startDate = this.start_in_date;
            endDate = this.end_in_date;
        } else {
            startDate =
                this.sdate.getFullYear() +
                '-' +
                (this.sdate.getMonth() + 1) +
                '-' +
                this.sdate.getDate();
            endDate =
                this.edate.getFullYear() +
                '-' +
                (this.edate.getMonth() + 1) +
                '-' +
                this.edate.getDate();
        }

        let rs: any;
        try {
            if (this.cid) {
                rs = await this.ketReferoutService.selectCid(
                    this.cid,
                    this.hcode,
                    'IN'
                );
            } else {
                rs = await this.ketReferoutService.selectReply(
                    this.hcode,
                    startDate,
                    endDate,
                    this.limitReferOut
                );
            }

            rs.forEach((v: any) => {
                if (
                    v.refer_appoint == '0' ||
                    v.refer_appoint == '1' ||
                    v.refer_appoint == '2' ||
                    v.refer_appoint == '4'
                ) {
                    this.rowsData1.push(v);
                }
            });

            rs.forEach((v: any) => {
                if (
                    v.refer_appoint == '0' ||
                    v.refer_appoint == '1' ||
                    v.refer_appoint == '2' ||
                    v.refer_appoint == '3' ||
                    v.refer_appoint == '4'
                ) {
                    this.rowsData2.push(v);
                }
            });
            this.loading = false;
            this.onCheckedAppoint(false);
        } catch (error) {
            console.log(error);
            this.loading = false;
        }
    }

    onCheckedAppoint(e: any) {

        let isChecked = e.checked;

        if (isChecked) {
            this.rowsData = this.rowsData2;
            this.strChecked1 = ' แสดงมีนัดแล้ว';

        } else {
            this.rowsData = this.rowsData1;
            this.strChecked1 = ' ปิดมีนัดแล้ว';

        }
    }

    async onCreate() {
        // console.log('create');
    }

    spclty(i: any) {
        this.itemeStation.forEach((e: any) => {
            if ((e.station_id = i)) {
                this.spclty_name = e.station_name;
            }
        });
    }

    onSearch() {

        this.justifyOptions = [
            { icon: 'pi pi-align-left', justify: 'Left' },
            { icon: 'pi pi-align-right', justify: 'Right' },
            { icon: 'pi pi-align-center', justify: 'Center' },
            { icon: 'pi pi-align-justify', justify: 'Justify' },
        ];
        this.stateOptions = [
            { label: 'Off', value: 'off' },
            { label: 'On', value: 'on' },
        ];

        sessionStorage.setItem('start_in_date', JSON.stringify(this.sdate));
        sessionStorage.setItem('end_in_date', JSON.stringify(this.edate));

        this.getInfo();
    }

    async onRowSelect(event: any) {

        if (event.data.receive_no == 0) {
            sessionStorage.setItem('itemStorage', JSON.stringify(event.data));
            this.router.navigate(['/home/referin-views']);

            sessionStorage.setItem('routmain', '/home/referout-views-only');

        } else {
            sessionStorage.setItem('itemStorage', JSON.stringify(event.data));
            this.router.navigate(['/home/referout-views-only']);

            sessionStorage.setItem('routmain', '/home/referin');

        }
    }

    async onReferIn(i: never) {
        sessionStorage.setItem('itemStorage', JSON.stringify(i));
        this.router.navigate(['/home/referin-views']);
        sessionStorage.setItem('routmain', '/home/referin');
    }

    reset() {
        this.first = 0;
    }

    isLastPage(): boolean {
        return this.rowsData
            ? this.first === this.rowsData.length - this.rows
            : true;
    }

    isFirstPage(): boolean {
        return this.rowsData ? this.first === 0 : true;
    }
    next() {
        this.first = this.first + this.rows;
    }

    prev() {
        this.first = this.first - this.rows;
    }
    uploadsRoute(datas: any) {
       
        let strdata: any = JSON.stringify(datas);
        sessionStorage.setItem('strdata', strdata);
        this.goToLink('/home/uploads');
    }
    async downloadAttRoute(filename: any) {

        let download: any = await this.ketAttachmentService.download(filename);
    }

    onClickSssess(datas: any) {
        let Storage: any = JSON.stringify(datas);
        sessionStorage.setItem('itemStorage', Storage);
        this.router.navigate(['/home/assess-view']);
    }

    async showDetail(data: any) {
        let ptName = data.fullname;

        try {

            let rs: any = await this.servicesService.patient(data.cid);

            this.textHN = rs.data.hn;
            this.content =
                '  ชื่อ-สกุล : ' + ptName + '<br>' + 'HN : ' + this.textHN;

            this.displayDetail = true;

            this.confirmationService.confirm({
                message: this.content,
                header: 'ข้อมูล',
                icon: 'pi pi-exclamation-triangle',
                accept: () => {
                    // this.msgs = [{severity:'info', summary:'Confirmed', detail:'You have accepted'}];
                    this.copyText();
                },
                reject: (type) => {
                    switch (type) {
                        case ConfirmEventType.REJECT:
                            this.msgs = [
                                {
                                    severity: 'error',
                                    summary: 'Rejected',
                                    detail: 'You have rejected',
                                },
                            ];
                            break;
                        case ConfirmEventType.CANCEL:
                            this.msgs = [
                                {
                                    severity: 'warn',
                                    summary: 'Cancelled',
                                    detail: 'You have cancelled',
                                },
                            ];
                            break;
                    }
                },
            });
        } catch (error) {
            console.log(error);
            this.displayDetail = true;

            this.confirmationService.confirm({
                message: 'เกิดข้อผิดพลาด',
                header: 'ข้อมูล',
                icon: 'pi pi-exclamation-triangle',
                accept: () => {
                    this.msgs = [
                        {
                            severity: 'info',
                            summary: 'Confirmed',
                            detail: 'You have accepted',
                        },
                    ];

                },
                reject: (type) => {
                    switch (type) {
                        case ConfirmEventType.REJECT:
                            this.msgs = [
                                {
                                    severity: 'error',
                                    summary: 'Rejected',
                                    detail: 'You have rejected',
                                },
                            ];
                            break;
                        case ConfirmEventType.CANCEL:
                            this.msgs = [
                                {
                                    severity: 'warn',
                                    summary: 'Cancelled',
                                    detail: 'You have cancelled',
                                },
                            ];
                            break;
                    }
                },
            });
        }
    }
    copyText() {
        this.clipboardApi.copyFromContent(this.content);
    }

    exportExcel() {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(this.rowsData);
            const workbook = {
                Sheets: { data: worksheet },
                SheetNames: ['data'],
            };
            const excelBuffer: any = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array',
            });
            this.saveAsExcelFile(excelBuffer, 'referin');
        });
    }

    saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE =
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE,
        });
        FileSaver.saveAs(
            data,
            fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
        );
    }

    async exportexcel2() {

        let checkT = await this.reDraw();
        if (checkT) {
            let fileName = 'ExcelSheet.xlsx';
            /* table id is passed over here */
            let element = document.getElementById('dt');
            const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

            /* generate workbook and add the worksheet */
            const wb: XLSX.WorkBook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

            /* save to file */
            XLSX.writeFile(wb, fileName);
        }
    }

    async reDraw() {
        this.paginator = false;
        this.rowsDataTemp = this.rowsData;
        this.rowsData = [];
        this.rowsData = this.rowsDataTemp;
        return true;
    }
    async exportExcel3() {
        // this.rowsDataTemp = this.rowsData;
        let fileName = 'ExcelSheet.xlsx';
        /* table id is passed over here */
        let element = document.getElementById('dtTemp');
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        XLSX.writeFile(wb, fileName);
    }

    valuechange(event: any) {
        if (event.target.value.length == 13) {
            this.validateForm = true;
        }
    }
    connectWebSocket() {
        // const rnd = new Random();
        const clientId = `smartrefer-${new Date().getTime()}`;

        try {
            this.client = mqttClient.connect(this.notifyUrl, {
                clientId: clientId,
                username: this.notifyUser,
                password: this.notifyPassword,
            });
        } catch (error) {
            console.log(error);
        }

        const topic = `smartrefer/${this.hcode}`;

        const that = this;

        this.client.on('message', async (topic, payload) => {
            try {
                const _payload = JSON.parse(payload.toString());
                if (_payload.refer_no) {
                    //load datas
                    this.getInfo();
                } else {
                    // this.clearData();
                }
            } catch (error) {
                console.log(error);
            }
        });

        this.client.on('connect', () => {
            // console.log(`Connected!`);
            that.zone.run(() => {
                that.isOffline = false;
            });

            that.client.subscribe(topic, { qos: 0 }, (error) => {
                if (error) {
                    that.zone.run(() => {
                        that.isOffline = true;
                        try {
                            // that.counter.restart();
                        } catch (error) {
                            console.log(error);
                        }
                    });
                } else {
                    // console.log(`subscribe ${topic}`);
                }
            });
        });

        this.client.on('close', () => {
            // console.log('MQTT Conection Close');
        });

        this.client.on('error', (error) => {
            // console.log('MQTT Error');
            that.zone.run(() => {
                that.isOffline = true;
                // that.counter.restart();
            });
        });

        this.client.on('offline', () => {
            // console.log('MQTT Offline');
            that.zone.run(() => {
                that.isOffline = true;
                try {
                    // that.counter.restart();
                } catch (error) {
                    console.log(error);
                }
            });
        });
    }
    goToLink(link:any) {
        const url = this.globalVariablesService.urlSite+link
    
        window.open(url, '_blank');
      }
}
