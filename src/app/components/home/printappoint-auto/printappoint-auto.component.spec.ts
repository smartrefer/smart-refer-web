import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintappointComponent } from './printappoint-auto.component';

describe('PrintappointComponent', () => {
  let component: PrintappointComponent;
  let fixture: ComponentFixture<PrintappointComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintappointComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintappointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
