import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import {SharedModule} from './shared/sharedModule';

import { environment } from '../environments/environment';
import { GlobalVariablesService } from './shared/globalVariables.service';

import { BlockViewer } from './components/blockviewer/blockviewer.component'; 
import { AppCodeModule } from './components/app-code/app.code.component';
import { AppComponent } from './app.component';
import { AppMainComponent } from './app.main.component';
import { AppTopBarComponent } from './app.topbar.component';
import { AppFooterComponent } from './app.footer.component';
import { AppConfigComponent } from './app.config.component';
import { AppMenuComponent } from './app.menu.component';
import {AppMenuTopComponent} from './app.menutop.component';
import { AppMenuitemComponent } from './app.menuitem.component';
import {AppMenuitemTopComponent} from './app.menuitemtop.component';

import { DashboardComponent } from './components/dashboard/dashboard.component';

import { CountryService } from './service/countryservice';
import { CustomerService } from './service/customerservice';
import { EventService } from './service/eventservice';
import { IconService } from './service/iconservice';
import { NodeService } from './service/nodeservice';
import { PhotoService } from './service/photoservice';
import { ProductService } from './service/productservice';
import { MenuService } from './service/app.menu.service';
import { ConfigService } from './service/app.config.service';

import {HomeComponent} from './components/home/home.component';

import { JwtModule } from '@auth0/angular-jwt';


export function tokenGetter() {
    return sessionStorage.getItem('token');
  }
  export const whitelistedDomains = [new RegExp('[\s\S]*')] as RegExp[];
  
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        SharedModule,
        AppCodeModule,
        JwtModule.forRoot({
            config: {
              tokenGetter: tokenGetter
            }
          }),
       
    ],
    declarations: [
        AppComponent,
        AppMainComponent,
        AppTopBarComponent,
        AppFooterComponent,
        AppConfigComponent,
        AppMenuComponent,
        AppMenuTopComponent,
        AppMenuitemComponent,
        DashboardComponent,      
        BlockViewer,
        AppMenuitemTopComponent,
        HomeComponent
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        CountryService, CustomerService, EventService, IconService, NodeService,
        PhotoService, ProductService, MenuService, ConfigService,
        { provide: 'API_URL', useValue: environment.apiUrl },
        { provide: 'COC_URL', useValue: environment.cocUrl },GlobalVariablesService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
