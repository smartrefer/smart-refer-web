import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintreferoutComponent } from './printreferout.component';

describe('PrintreferoutComponent', () => {
  let component: PrintreferoutComponent;
  let fixture: ComponentFixture<PrintreferoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintreferoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintreferoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
