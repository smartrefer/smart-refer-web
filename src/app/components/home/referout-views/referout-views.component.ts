import * as moment from 'moment-timezone';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';


import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';


//lookup 
import { ServicesService } from '../../../services-api/services.service';
import { KetTypeptService } from '../../../services-api/ket-typept.service';
import { KetStrengthService } from '../../../services-api/ket-strength.service';
import { KetLoadsService } from '../../../services-api/ket-loads.service';
import { KetThaiaddressService } from '../../../services-api/ket-thaiaddress.service';
import { KetReferoutService } from '../../../services-api/ket-referout.service';
import { KetAttachmentService } from '../../../services-api/ket-attachment.service';
import { AlertService } from '../../../service/alert.service';
import { KetStationService } from 'src/app/services-api/ket-station.service';
import { KetAppointSlotService} from 'src/app/services-api/ket-appoint-slot.service';
import { log } from 'console';

// import { AnyMxRecord } from 'dns';


@Component({
  selector: 'app-referout-views',
  templateUrl: './referout-views.component.html',
  styleUrls: ['./referout-views.component.css']
})
export class ReferoutViewsComponent implements OnInit {
  itemStorage: any = [];
  itematt: any = [];

  file: File | null = null
  uploadfile:any;
  att_type:any;
  att_name:any;

  itemeTypept: any = [];
  itemeStrength: any = [];
  itemeLoads: any = [];
  itemeStation: any = [];
  itemeRefertriage:any=[];
  itemeRefertype:any=[];

  itemeAppointSlot: any = [];
  
  appointDate: any;
  isSelectClinic: boolean = false;

  routmain:string;

  itemeAttachment:any=[];
  loadingAttachment: boolean = true;

  username: any;
  user_fullname: any;

  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];

  today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  totime: any = moment(Date()).tz('Asia/Bangkok').format('HH:mm:ss');

  blockedPanel: boolean = false;
  showsappointslot: boolean = false;

  selectedRowsData: any = [];

  statuses: any[] = [];

  loading: boolean = true;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  displayReferOut: boolean = false;
  rowsDataReferOut: any[] = [];
  loadingReferOut: boolean = true;

  @ViewChild('dt') table!: Table;

  @ViewChild('dtCreate') tableCreate!: Table;

  first: any = 0;

  rows: any = 10;

  hcode: any;
  refer_no_ : any;
  sdate: any;
  edate: any;

  sdateCreate: any;
  edateCreate: any;

  limitReferOut: any = 2000;


  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = "off";
  stateOptions: any = [];

  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';
  itemreferout: any;

  allergy: any = [];
  appointment: any = [];
  diagnosis: any = [];
  drugs: any = [];
  hpi: any = {};
  lab: any = [];
  medrecconcile: any = [];
  nurtures: any = {};
  pe: any = {};
  procedure: any = [];
  profile: any = {};
  refer: any = {};
  vaccines: any = [];
  xray: any = [];

  adddresss: any = {};
  diag_text: any;


  typept_id: any = '';
  strength_id: any = '';
  loads_id: any = '';
  station_id:any = '';

  clinic_code:any = '';
  clinic_name:any = '';

  refer_triage_id: any = '';
  location_refer_id: any = '';
  refer_type: any = '';
  expire_date: any;
  equipwithpatient:any;
  refer_remark:any;


  bmi: any;
  refer_appoint: any = 0;
  refer_xray_online: any = 0;


  textCc: any;
  textPmh: any;
  textPe: any;
  textHpi: any;

  textAddress: any
  validateForm:boolean = false;
  refer_sentto_CancerAnywhere: any = 0;
 
  // apooint_telemed: string;
  apooint_telemed: any = 0;

  itemeCovidVaccine: any = [];

  public telemed_time = ''
  public maskTime = [ /\d/, /\d/, ':', /\d/, /\d/]
  blockedDocument: boolean = false;


  constructor(
    private primengConfig: PrimeNGConfig,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private ketTypeptService: KetTypeptService,
    private ketStrengthService: KetStrengthService,
    private ketLoadsService: KetLoadsService,
    private ketThaiaddressService: KetThaiaddressService,
    private ketReferoutService: KetReferoutService,
    private ketAttachmentService: KetAttachmentService,
    private alertService:AlertService,
    private ketStationService: KetStationService,
    private ketAppointSlotService:KetAppointSlotService,


  ) {

    this.username = sessionStorage.getItem('username');
    this.user_fullname = sessionStorage.getItem('fullname');
    this.hcode = sessionStorage.getItem('hcode');


    this.sdateCreate = this.today;
    this.edateCreate = this.today;
    this.routmain=sessionStorage.getItem('routmain');

  }

  ngOnInit(): void {
    this.readGlobalValue() ;
    let Typept:any = localStorage.getItem('itemeTypept');
    this.itemeTypept = JSON.parse(Typept);

    
    let Strength:any = localStorage.getItem('itemeStrength');
    this.itemeStrength = JSON.parse(Strength);

    let Loads:any = localStorage.getItem('itemeLoads');
    this.itemeLoads = JSON.parse(Loads);

    let AppointSlot:any = localStorage.getItem('itemeAppointSlot');
    this.itemeAppointSlot = JSON.parse(AppointSlot);

    let Refertriage:any = localStorage.getItem('itemeRefertriage');
    this.itemeRefertriage = JSON.parse(Refertriage);


    let Refertype:any = localStorage.getItem('itemeRefertype');
    this.itemeRefertype = JSON.parse(Refertype);

    let i: any = sessionStorage.getItem('itemStorage');
    this.itemStorage = JSON.parse(i);

    if (!this.itemStorage) {
      this.router.navigate(['/home/referout-create']);
    } else {
      this.getReferOut(this.itemStorage);
    }
  }
  uploadsRoute(){
    let datas = this.itemStorage;
    let strdata:any = JSON.stringify(datas);
    sessionStorage.setItem('strdata',strdata);
    this.goToLink('/home/uploads');
}

goToLink(link:any) {

  const url = this.globalVariablesService.urlSite+link
  window.open(url, '_blank');
}

    // Medthod  ที่ต้องเหมือนกันทุก component/////////////////////
    readGlobalValue() {
      this.scrHeight = Number(this.globalVariablesService.scrHeight);
      this.scrWidth = Number(this.globalVariablesService.scrWidth);
      this.boxHeight = ((this.scrHeight) - 80) + 'px';
      // กำหนด ความสูง  ความกว้างของตาราง 
      this.ptableStyle = {
        // width: (this.scrWidth-20) + 'px',
        width: '100%',      
        // height: (this.scrHeight - 400) + 'px'
      }  
    }
    async getStation(hospcode:any) {
      try {
        let rs: any = await this.ketStationService.select_hospcode(hospcode);
        this.itemeStation.push({ station_id: '', station_name: 'กรุณาเลือก' ,hospcode:''});
        rs.forEach((e: any) => {
          this.itemeStation.push(e);
        });
      } catch (error) {
        console.log('itemeStation',error);
  
      }
    }

    async getAppointSlot(to_hcode:any) {
      try {
        let rs: any = await this.ketAppointSlotService.appointslots(to_hcode);

        if (rs.length >0){
          this.itemeAppointSlot = rs.filter((res:any)=> res.is_active == '1');

          if(this.itemeAppointSlot.length > 0){
            this.showsappointslot = true;
          }

        }
      } catch (error) {
        console.log('itemeAppointSlot',error);
  
      }
    }

  onTimeChange(e){
    this.telemed_time = e;
  }

  onDateSelected(){

    if(this.edate < this.sdate){     
      this.alertService.error("วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้","ข้อผิดพลาด !");
      this.validateForm = false;
    }else{
      this.validateForm = true;
    }
  }

  referData:any = [];
  async getReferOut(i: any) {
  
    try {
      let rs: any = await this.servicesService.view(i.hn, i.seq, i.referno,);
      // console.log(rs);
      this.referData = rs;
      if (rs) {

        let nurture:object;

        if(rs.nurtures[0]){
          nurture = rs.nurtures[0];
        }

        this.allergy = rs.allergy;
        this.appointment = rs.appointment;
        this.diagnosis = rs.diagnosis;
        this.drugs = rs.drugs;
        this.hpi = rs.hpi[0];
        this.lab = rs.lab;
        this.medrecconcile = rs.medrecconcile;
        this.nurtures = nurture;
        this.pe = rs.pe[0];
        this.procedure = rs.procedure;
        this.profile = rs.profile[0];
        this.refer = rs.refer[0];
        this.vaccines = rs.vaccines;
        this.xray = rs.xray;

        this.typept_id= `${this.refer.pttype_id}`;
        this.strength_id = this.refer.strength_id;
        this.loads_id = this.refer.load_id;

        this.getStation(this.refer.to_hcode);
        this.getAppointSlot(this.refer.to_hcode);
        // console.log(this.profile);
        
        this.expire_date = moment(new Date().getTime() + 365 * 86400E3).tz('Asia/Bangkok').format('YYYY-MM-DD')

        if(this.nurtures){
          this.diag_text = this.nurtures.diag_text;
          this.textCc = this.nurtures.cc;
          this.textPmh = this.nurtures.pmh;
          if ((this.nurtures.weight != '' && this.nurtures.weight != null) && (this.nurtures.height != '' && this.nurtures.height != null)) {
            this.bmi = (this.nurtures.weight / ((this.nurtures.height / 100) * (this.nurtures.height / 100)));
          }  
        }
        
        if (this.pe) {
          this.textPe = this.pe.pe;
        }
        if (this.hpi) {
          this.textHpi = this.hpi.hpi;
        }



        if (this.profile.chwpart && this.profile.amppart && this.profile.tmbpart) {
          if (this.profile.moopart && this.profile.moopart != "" && this.profile.moopart != "00") {
            this.getAddress_full(this.profile.chwpart, this.profile.amppart, this.profile.tmbpart, this.profile.moopart);
          } else {
            this.getAddress(this.profile.chwpart, this.profile.amppart, this.profile.tmbpart);
          }
        }
        this.refer_no_ = `${this.hcode}-1-${i.referno}`
        this.getAttachment(this.refer_no_);

        //ปิดระบบ
        // this.getCovidVaccine(this.profile.cid);
        // console.log(this.bmi);

      } else {


      }
    } catch (error) {
      console.log(error);
      // this.alertService.error(JSON.stringify(error),'ข้อมูลผิดพลาด');
  
      // this.alertService.error();
    }
  }

  async getAddress_full(chwpart: any, amppart: any, tmbpart: any, moopart: any) {
    try {
      let rs: any = await this.ketThaiaddressService.select_full(chwpart, amppart, tmbpart, moopart);
      this.textAddress = rs[0].full_name
    } catch (error) {
      console.log('error',error);
    }
  }

  async getAddress(chwpart: any, amppart: any, tmbpart: any) {
    try {
      let rs: any = await this.ketThaiaddressService.select(chwpart, amppart, tmbpart);
      this.textAddress = rs[0].full_name
    } catch (error) {
      console.log('error',error);
    }
  }

  async onChangeSave(){
    this.blockedDocument = true;
    if(this.typept_id && this.strength_id && this.loads_id && this.refer_triage_id && this.station_id && this.refer_type ){
      this.onSave();
    }else{
    this.blockedDocument = false;

      this.alertService.error('เลือกข้อมูลไม่ครบ กรุณาเลือกข้อมูลใหม่');
    }
  }

  async onSave() {

    if(!this.refer.ReferTime){
      this.alertService.info('ข้อมูล เวลาการ Refer ไม่ได้ลงใน HIS ')
    }else if(!this.refer.ReferDate){
      this.alertService.info('ข้อมูล วันที่ Refer ไม่ได้ลงใน HIS ')
    }else if(!this.profile.age){
      this.alertService.info('ข้อมูล อายุ หรือ วันเกิด ไม่ได้ลงใน HIS ')
    }else if(!this.profile.tmbpart){
      this.alertService.info('ข้อมูล ตำบล ไม่ได้ลงใน HIS ')
    }else if(!this.profile.amppart){
      this.alertService.info('ข้อมูล อำเภอ ไม่ได้ลงใน HIS ')
    }else if(!this.profile.chwpart){
      this.alertService.info('ข้อมูล จังหวัด ไม่ได้ลงใน HIS ')
    }else if(!this.refer.to_hcode){
      this.alertService.info('ข้อมูล โรงพยาบาลบริการปลายทาง ไม่ได้ลงใน HIS ')
    }else if(!this.profile.pttype_name){
      this.alertService.info('ข้อมูล สิทธิการรักษา ไม่ได้ลงใน HIS ')
    // }else if(!this.profile.hospmain && !this.profile.hospsub){
    //   this.alertService.info('ข้อมูล สถานพยาบาลหลัก หรือ สถานพยาบาลรอง ไม่ได้ลงใน HIS ')
    }else if(this.clinic_code && !this.appointDate){
      this.alertService.error('เลือกข้อมูลไม่ครบ กรุณาระบุวันขอนัด');

    }else{

      let dataPost:any;
      let refer_no_:any = `${this.refer.provider_code}-1-${this.refer.referno}`
  
  
      let typept_name:any;
      this.itemeTypept.forEach((e:any) => {
        if(e.typept_id == this.typept_id){
          typept_name = e.typept_name;
        }
      })
  
      let strength_name:any;
      this.itemeStrength.forEach((e:any) => {
        if(e.strength_id == this.strength_id){
          strength_name = e.strength_name;
        }
      })
  
      let loads_name:any;
      this.itemeLoads.forEach((e:any) => {
        if(e.loads_id == this.loads_id){
          loads_name = e.loads_name;
        }
      })
  
      let station_name:any;
      this.itemeStation.forEach((e:any) => {
        if(e.station_id == this.station_id){
          station_name = e.station_name;
        }
      })
  
      let refer_triage_name:any;
      let location_refer_name:any;
      let refer_type_name:any;
  
      let diagnosis_ :any=[];
      let drugs_ :any=[];
      let medrecconcile_ :any=[];
      let allergy_ :any=[];
      let lab_ :any=[];
      let xray_ :any=[];
      let procedure_ :any=[];
      
      
      let _diagnosis_ :any=[];
      let _drugs_ :any=[];
      let _medrecconcile_ :any=[];
      let _allergy_ :any=[];
      let _lab_ :any=[];
      let _xray_ :any=[];
      let _procedure_ :any=[];
  
      _diagnosis_ = this.diagnosis || [];
      _drugs_ = this.drugs || [];
      _medrecconcile_ = this.medrecconcile || [];
      _allergy_ = this.allergy || [];
      _lab_ = this.lab || [];
      _xray_ = this.xray || [];
      _procedure_ = this.procedure || [];
  
  
      if(_diagnosis_[0]){
        this.diagnosis.forEach((e:any) => {
          let diagnosis = {
            "hcode": e.provider_code,
            "refer_no": refer_no_,
            "icd_code": e.icd_code,
            "icd_name": e.icd_name,
            "diag_type": e.diag_type,
            "diagnote": e.diagnote,
            "diagtype_id": e.diagtype_id
          }
          diagnosis_.push(diagnosis);
  
        });
      }
  
      if(_drugs_[0]){
        this.drugs.forEach((e:any) => {
          let drugs = {
            "hcode": e.provider_code,
            "refer_no": refer_no_,
            "seq": e.seq,
            "date_serv": e.date_serv,
            "time_serv": e.time_serv,
            "drug_name": e.drug_name,
            "qty": e.qty,
            "unit": e.unit,
            "usage_line1": e.usage_line1,
            "usage_line2": e.usage_line2,
            "usage_line3": e.usage_line3
          }
          drugs_.push(drugs);
        });
      }
  
      if(_medrecconcile_[0]){
        this.medrecconcile.forEach((e:any) => {
          let medrecconcile = {
            "hcode": e.provider_code,
            "refer_no": refer_no_,
            "drug_hospcode": e.drug_hospcode,
            "drug_hospname": e.drug_hospname,
            "drug_name": e.drug_name,
            "drug_use": e.drug_use,
            "drug_receive_date": e.drug_receive_date
          }
          medrecconcile_.push(medrecconcile);
        });
      }
  
      if(_allergy_[0]){
        this.allergy.forEach((e:any) => {
          let allergy = {
            "hcode": e.provider_code,
            "refer_no": refer_no_,
            "hname": e.hname,
            "drug_name": e.drug_name,
            "symptom": e.symptom,
            "begin_date": e.begin_date
          }
          allergy_.push(allergy);
  
        });
      }
  
  
      if(_lab_[0]){
        this.lab.forEach((e:any) => {
          let lab = {
            "hcode": e.provider_code,
            "refer_no": refer_no_,
            "date_serv": e.date_serv,
            "time_serv": e.time_serv,
            "labgroup": e.labgroup,
            "lab_name": e.lab_name,
            "lab_result": e.lab_result,
            "unit": e.unit,
            "standard_result": e.standard_result
          }
          lab_.push(lab);
  
        });
      }
  
      if(_xray_[0]){
        this.xray.forEach((e:any) => {
          let xray = {
            "refer_no": refer_no_,
            "xray_date": e.xray_date,
            "xray_name": e.xray_name
          }
          xray_.push(xray);
  
        });
      }
  
      if(_procedure_[0]){
        this.procedure.forEach((e:any) => {
          let procedure = {
            "hcode": e.provider_code,
            "refer_no": refer_no_,
            "seq": e.seq,
            "date_serv": e.date_serv,
            "time_serv": e.time_serv,
            "procedure_code": e.procedure_code,
            "procedure_name": e.procedure_name,
            "start_date": e.start_date,
            "end_date": e.end_date
          }
          procedure_.push(procedure);
  
        });
      }
  
  
      let telemeddate =  moment(this.sdate).tz('Asia/Bangkok').format('YYYY-MM-DD');

      dataPost = {
        "hospital": {
          "providerCode": this.refer.provider_code,
          "providerName": this.refer.provider_name,
          "providerUser": this.username
        },
        "referout": {
          "hcode": this.refer.provider_code,
          "refer_no": refer_no_,
          "refer_date": this.refer.ReferDate,
          "refer_time": this.refer.ReferTime,
          "station_id": this.refer.station_id,
          "station_name": this.refer.station_name,
          "location_id": this.profile.location_id,
          "location_name": this.refer.location_name,
  
          "cid": this.profile.cid,
          "an": this.refer.an,
          "hn": this.refer.hn,
          "vn": this.refer.seq,
          "pid": this.refer.pid,
  
          "pname": this.profile.title_name,
          "fname": this.profile.first_name,
          "lname": this.profile.last_name,
          "sex": this.profile.sex,
          "occupation": this.profile.occupation,
          "dob": this.profile.brthdate,
          "age": this.profile.age,
          "father_name": this.profile.father_name,
          "mother_name": this.profile.mother_name,
          "couple_name": this.profile.couple_name,
          "contact_name": this.profile.contact_name,
          "contact_relation": this.profile.contact_relation,
          "contact_mobile": this.profile.contact_mobile,
          "addrpart": this.profile.addrpart,
          "moopart": this.profile.moopart,
          "tmbpart": this.profile.tmbpart,
          "amppart": this.profile.amppart,
          "chwpart": this.profile.chwpart,
          "pttype_id": this.profile.pttype_id,
          "pttype_name": this.profile.pttype_name,
          "pttypeno": this.profile.pttype_no,
          "hospmain": this.profile.hospmain,
          "hospsub": this.profile.hospsub,
          "typept_id": this.typept_id,
          "typept_name": typept_name,
          "strength_id": this.strength_id,
          "strength_name": strength_name,
          "doctor_id": this.refer.doctor,
          "doctor_name": this.refer.doctor_name,
          "refer_hospcode": this.refer.to_hcode,
          "cause_referout_id": this.refer.cause_referout_id,
          "cause_referout_name": this.refer.refer_cause,
  
          "expire_date": this.expire_date,
          "loads_id": this.loads_id,
          "loads_name": loads_name,
          "refer_remark": this.refer_remark,
          "equipwithpatient": this.equipwithpatient,
          "refer_triage_id": this.refer_triage_id,
          "refer_triage_name": refer_triage_name,
          "location_refer_id": this.station_id,
          "location_refer_name": station_name,
          "refer_type": this.refer_type,
          "refer_type_name": refer_type_name,
          "refer_appoint": `${this.refer_appoint}`,
          "refer_xray_online": this.refer_xray_online,
          "refer_his_no": this.refer.referno,
          "refer_station_id": this.refer.refer_station_id,
          "refer_station_name": this.refer.efer_station_name,
          "serviceplan_id": "",
          "serviceplan_name": "",
          "receive_no": "",
          "receive_date": "",
          "receive_time": "",
          "receive_station_id": "",
          "receive_station_name": "",
          "receive_spclty_id": "",
          "receive_spclty_name": "",
          "receive_refer_result_id": "",
          "receive_refer_result_name": "",
          "receive_serviceplan_id": "",
          "receive_serviceplan_name": "",
          "receive_ward_id": "",
          "receive_ward_name": "",
          "refer_sentto_CancerAnywhere":this.refer_sentto_CancerAnywhere,
          "apooint_telemed":this.apooint_telemed,
  
          "tele_med_date" : telemeddate,
          "tele_med_time" : this.telemed_time+':00'
        },
        "rfdiag": diagnosis_,
        "rfdrug": drugs_,
        "medrecconcile": medrecconcile_,
        "rfallergy": allergy_,
        "rflab": lab_,
        "xray": xray_,
        "rfprocedure": procedure_,
        "sign_text": [
          {
            "hcode": this.nurtures.provider_code || '',
            "refer_no": refer_no_,
            "body_weight_kg": this.nurtures.weight || '',
            "height_cm": this.nurtures.height || '',
            "pds": this.nurtures.dbp || '',
            "bps": this.nurtures.sbp || '',
            "temperature": this.nurtures.temperature || '',
            "hr": this.nurtures.pr || '',
            "pulse": this.nurtures.pulse || '',
            "rr": this.nurtures.rr || '',
            "bloodgroup": this.nurtures.bloodgrp || '',
            "cc": this.textCc,
            "pe": this.textPe,
            "hpi": this.textHpi,
            "pmh": this.textPmh,
            "treatment": this.nurtures.treatment || '',
            "oxygen_sat": this.nurtures.oxygen_sat || '',
            "eye_score": this.nurtures.eye_score,
            "movement_score": this.nurtures.movement_score || '',
            "vocal_score": this.nurtures.vocal_score || '',
            "pupil_right": this.nurtures.pupil_right || '',
            "pupil_left": this.nurtures.pupil_left || '',
            "diag_text": this.diag_text
          }
        ]
      }
  
      let info:any={};
      info.rows = dataPost;

      try {
        let rs:any = await this.ketReferoutService.onSeve(info)

        if(rs.res_.ok == true){
          if(this.refer_appoint == 3){
            this.updateReferAppoint(refer_no_);
          }

          // บันทึกข้อมูลวันนัด
          if(this.clinic_code && this.appointDate){
            let data: any = {};
            data.rows = this.appoint;

            let rs:any = await this.ketAppointSlotService.onSaveAppoint(data)

          }

          this.blockedDocument = false;
          this.alertService.successPro('บันทึกสำเร็จเรียบร้อย','ดำเนินการเสร็จเรียบร้อย');
          this.router.navigate([sessionStorage.getItem('routmain')]);

        }else{

          this.blockedDocument = false;
          console.log('error',rs.res_.error);
          if(rs.res_.error.code == 'ER_DUP_ENTRY'){
            this.alertService.error(`เลขรีเฟอร์ ${refer_no_} ถูกใช้งานส่งรีเฟอร์ไปแล้ว`,'เกิดข้อผิดพลาด');
          }else{
            this.alertService.error(JSON.stringify(rs.res_.error),'บันทึกสำเร็จไม่สำเร็จ');
          }
        }
        
      } catch (error) {

        this.blockedDocument = false;
        console.log('error',error);
        this.alertService.error(JSON.stringify(error),'ข้อมูลผิดพลาด');  
  
      }
  
    }

  }

  onFileInput(files: FileList | null): void {
    if (files) {
      this.file = files.item(0)
    }
  }

  async onUpload() {

    let refer_no:any = `${this.refer.provider_code}-1-${this.refer.referno}`
    let info:any = {
      "att_type":this.att_type,
      "att_name":this.att_name
    }
    
    if (this.file) {
      let subscription:any = await this.ketAttachmentService.upload(this.file,refer_no,info);
      this.uploadfile = subscription.file.path;
      
    }
  }

  async getAttachment(i:any) {
    this.loadingAttachment = true;
    try {
      let rs: any = await this.ketAttachmentService.select(i);
      if (rs[0]) {
        this.itemeAttachment = rs;
        this.loadingAttachment = false;
      } else {
        this.loadingAttachment = false;
      }
    } catch (error) {
      console.log(error);
      this.loadingAttachment = false;
    }
    
  }
  
  async downloadAttRoute(filename: any){
    let download: any = await this.ketAttachmentService.download(filename);
  }

  async updateReferAppoint(refer_no:any){
    let i:any = {};
    let info:any = {
      "referout": {
        "receive_no": refer_no,
        "receive_date": this.today,
        "receive_time": this.totime,
        "receive_station_id": "",
        "receive_station_name": "",
        "receive_spclty_id": "1",
        "receive_spclty_name": "OPD",
        "receive_refer_result_id": "1",
        "receive_refer_result_name": "ตอบรับการส่งต่อ",
        "receive_serviceplan_id": "99",
        "receive_serviceplan_name": "อื่นๆ",
        "reject_refer_reason": "",
        "receive_ward_id": "",
        "receive_ward_name": ""
        }
    }
    i.rows = info;
    try {
      let rs: any = await this.ketReferoutService.onUpdate(i,refer_no);
    } catch (error) {
      console.log(error);
      this.alertService.error(JSON.stringify(error),'ข้อมูลผิดพลาด');
  
    }
  }

  async getCovidVaccine(i: any) {
    try {
      let rs: any = await this.ketReferoutService.covidvaccine(i);
      if(rs.statusCode == 200){
        this.itemeCovidVaccine = rs['info']['result']['vaccine_history'];
      }
    } catch (error) {
      console.log('ไม่พบรายการ');
    }
  }

  holiday: any = [];
  async getHoliday() {
    this.holiday = await this.ketAppointSlotService.getHoliday();
  }

  aviailableDate: {
    date: any,
    timeOpen: any,
    name: any
  }[] = [];
  isFinishCalculate: boolean = false;
  async calculateAvialableDate(dayOpen:any,timeOpen:any,maxPatient:number) {

    let maxDayToAppoint = 30; // จำนวนวันที่สามารถนัดหมายล่วงหน้าได้
    let dateAvialble = []; // วันที่ที่สามารถนัดหมาย

    for(let i = 1; i <= maxDayToAppoint; i++) {
      let date = new Date(); // วันที่ปัจจุบัน
      date.setDate(date.getDate() + i); // วันที่ปัจจุบัน + 1
      let day = moment(date).format('YYYY-MM-DD'); // วันที่ปัจจุบัน + 1 ในรูปแบบ YYYY-MM-DD
      let dayOfWeek = moment(date).day()+1; // วันในสัปดาห์ 1-7
      let isHoliday: boolean = this.checkHoliday(day); // ตรวจสอบว่าเป็นวันหยุดหรือไม่
      let isOpen:boolean = this.checkDayOpen(dayOfWeek,dayOpen); // ตรวจสอบว่าเปิดให้นัดหมายหรือไม่
      let isFull:boolean = false; // ตรวจสอบว่าเต็มหรือไม่
      let totalPatient = await this.getCountAppointment(this.hcode,this.clinic_code,day); // จำนวนคนที่นัดหมายในวันนั้น
      // ถ้าจำนวนคนที่นัดหมายในวันนั้นมากกว่าหรือเท่ากับจำนวนคนที่นัดหมายได้สูงสุด ให้เต็ม
      if(totalPatient >= maxPatient) {
        isFull = true;
      }
      // ถ้าไม่ใช่วันหยุด และเปิดให้นัดหมาย และยังมีว่าง ให้เพิ่มวันที่นัดหมาย
      if(!isHoliday && isOpen && !isFull) {
        let info = {
          date: day,
          name: date.toLocaleDateString('th-TH', { year: 'numeric', month: 'short', day: 'numeric', weekday: 'long' }) + ' (' + timeOpen + ')',
          timeOpen: timeOpen
        }
        dateAvialble.push(info);
      }
    }
    this.aviailableDate = dateAvialble; // วันที่ที่สามารถนัดหมาย
    this.isFinishCalculate = true;
    // console.log('avialable date:',this.aviailableDate); // วันที่ที่สามารถนัดหมาย

  }

  appoint:any={};
  onChangeDate(event:any){
    // console.log('appoint alot:',this.appointSlot);
    this.appoint = {
      "hcode": this.referData.refer[0].provider_code,
      "clinic_code": this.clinic_code,
      "refer_no": this.refer_no_,
      "appoint_hcode": this.referData.refer[0].to_hcode,
      "appoint_hname": this.referData.refer[0].to_hcode_name,
      "clinic_name": this.appointSlot[0].clinic_name,
      "clinic_contact": this.appointSlot[0].clinic_contact,
      "doctor_name": this.appointSlot[0].doctor_name,
      "patient_name": this.referData.profile[0].title_name + this.referData.profile[0].first_name + this.referData.profile[0].last_name,
      "cid": this.referData.profile[0].cid,
      "sex": this.referData.profile[0].sex,
      "age": this.referData.profile[0].age,
      "patient_contact": this.referData.profile[0].contact_mobile,
      "appoint_date": event,
      "appoint_time":this.appointSlot[0].time_open,
      "appoint_by": this.user_fullname,
      "description":this.appointSlot[0].description,
      "remark": this.appointSlot[0].remark,
    }

  }

  checkHoliday(date:any) {
    let isHoliday: boolean = false;
    // ตรวจสอบว่าเป็นวันหยุดหรือไม่
    for(let i = 0; i < this.holiday.length; i++) {
      if (date == this.holiday[i].date) {
        isHoliday = true;
        break; // ถ้าเป็นวันหยุด ให้หยุดตรวจสอบต่อ
      }
    }
    return isHoliday; // คืนค่าว่าเป็นวันหยุดหรือไม่
  }

  checkDayOpen(dayOfWeek:any,dayOpenArray:any) {
    // if dayofweek in dayOpenArray return true
    return dayOpenArray.includes(dayOfWeek.toString()); // ตรวจสอบว่าวันนี้เปิดให้นัดหมายหรือไม่
  }

  appointSlot:any = [];
  onChangeClinicCode(event:any){
    this.isFinishCalculate = false;
    this.appointSlot = this.itemeAppointSlot.filter((res:any)=> res.clinic_code == event); // ค้นหาข้อมูลของคลินิกที่เลือก
    let dayOpen = JSON.parse(this.appointSlot[0].day_open); // วันที่เปิดให้นัดหมาย
    let timeOpen = this.appointSlot[0].time_open; // เวลาที่เปิดให้นัดหมาย
    let maxPatient = this.appointSlot[0].max_patient; // จำนวนคนที่นัดหมายได้สูงสุด
    this.calculateAvialableDate(dayOpen,timeOpen,maxPatient); // คำนวณวันที่สามารถนัดหมาย
    this.isSelectClinic = true; // ตั้งค่าเป็นเลือกคลินิกแล้ว
  }

  async getCountAppointment(hcode:string,clinic_code:string,appoint_date:string) {
    let total = 0;
    let rs:any = await this.ketAppointSlotService.getCountAppoint(hcode,clinic_code,appoint_date); 
    total = rs.total;
    return total;
  }
}
