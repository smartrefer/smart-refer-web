import * as moment from 'moment-timezone';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ThisReceiver } from '@angular/compiler';

//lookup 
import { ServicesService } from '../../../services-api/services.service';
import { KetTypeptService } from '../../../services-api/ket-typept.service';
import { KetStrengthService } from '../../../services-api/ket-strength.service';
import { KetLoadsService } from '../../../services-api/ket-loads.service';
import { KetThaiaddressService } from '../../../services-api/ket-thaiaddress.service';
import { KetReferoutService } from '../../../services-api/ket-referout.service';
import { KetAttachmentService } from '../../../services-api/ket-attachment.service';
import { KetReferResultService } from '../../../services-api/ket-refer-result.service';
import { KetServiceplanService } from '../../../services-api/ket-serviceplan.service';
import { KetStationService } from '../../../services-api/ket-station.service';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-printreferout',
  templateUrl: './printreferout.component.html',
  styleUrls: ['./printreferout.component.css']
})
export class PrintreferoutComponent implements OnInit {
  username:any;
  itemStorage: any = [];
  itematt:any=[];

  itemeTypept: any = [];
  itemeStrength: any = [];
  itemeLoads: any = [];
  itemeStation: any = [];
  itemeRefertriage:any=[];
  itemeRefertype:any=[];
  itemeServiceplan:any=[];
  itemeResult:any=[];



  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];
  
  today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');

  blockedPanel: boolean = false;

  selectedRowsData:any = [];

  statuses: any[] = [];

  loading: boolean = true;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  displayReferOut: boolean = false;
  rowsDataReferOut: any[] = [];
  loadingReferOut: boolean = true;

  @ViewChild('dt') table!: Table;

  @ViewChild('dtCreate') tableCreate!: Table;

  first: any = 0;

  rows: any = 10;

  hcode: any;

  sdate: any;
  edate: any;

  sdateCreate: any;
  edateCreate: any;

  limitReferOut: any = 2000;


  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = "off";
  stateOptions: any = [];

  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';
  itemreferout:any;

  allergy:any=[];
  appointment:any=[];
  diagnosis:any=[];
  drugs:any=[];
  hpi:any={};
  lab:any=[];
  medrecconcile:any=[];
  nurtures:any=[];
  pe:any={};
  procedure:any=[];
  profile:any={};
  refer:any={};
  vaccines:any=[];
  xray:any=[];
  result:any=[];
  serviceplan:any=[];
  

  referOut:any={};
  signtext:any={};


  adddresss:any={};
  diag_text:any;


  typept_id:any;
  strength_id:any;
  loads_id:any;
  refer_triage_id: any = '';
  location_refer_id: any = '';
  refer_type: any = '';
  expire_date: any;

  refer_result_id:any;
  refer_result_name:any;
  serviceplan_id:any;
  receive_spclty_id:any;
  receive_ward_id:any;
  station_id:any;
  bmi:any;
  refer_appoint:any;
  refer_xray_online:any;
  station:any;
  department:any;
  refer_reject_reasons:any;
  doctor_license:any;





  textCc:any;
  textPmh:any;
  textPe:any;
  textHpi:any;

  textAddress:any
  selectedResultId:string = '';
  selectedServicePlanId:string = '';
  equipwithpatient:any; 
  refer_remark:any;

  buttonClose: any;

  blockedDocument: boolean = false;
  constructor(
    private primengConfig: PrimeNGConfig,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private ketTypeptService: KetTypeptService,
    private ketStrengthService: KetStrengthService,
    private ketLoadsService: KetLoadsService,
    private ketThaiaddressService: KetThaiaddressService,
    private ketReferoutService: KetReferoutService,
    private ketAttachmentService: KetAttachmentService,
    private ketReferResultService: KetReferResultService,
    private ketServiceplanService: KetServiceplanService,
    private ketStationService: KetStationService,
  ) { }

  ngOnInit(): void {
    this.lookupDatas();
    let i: any = sessionStorage.getItem('itemStorage');
        this.itemStorage = JSON.parse(i);

        this.getReferOut(this.itemStorage)
  }

  print() {
    let printContents = document.getElementById("print-section").innerHTML;
        let originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
        window.location.reload();
  }

  async getReferOut(i: any) {
    this.blockedDocument = true;
    try {
        let rs: any = await this.ketReferoutService.receive(i.refer_no,);

        if (rs) {
          this.allergy = rs.allergy;
          this.appointment = rs.appointment;
          this.diagnosis = rs.diagnosis;
          this.drugs = rs.drug;
          this.lab = rs.lab;
          this.medrecconcile = rs.medrecconcile;
          this.signtext = rs.signtext[0];
          
          this.procedure = rs.procedure;
          this.xray = rs.xray;
          this.referOut = rs.referout;         

          this.station_id = this.referOut.location_refer_id;
          this.typept_id = this.referOut.typept_id
          this.strength_id = this.referOut.strength_id
          this.loads_id = this.referOut.loads_id
          this.refer_triage_id = this.referOut.refer_triage_id;
          this.refer_type = this.referOut.refer_type;
          this.refer_appoint = +this.referOut.refer_appoint;
          this.refer_xray_online = +this.referOut.refer_xray_online;
          this.equipwithpatient = this.referOut.equipwithpatient;
          this.refer_remark = this.referOut.refer_remark;


          this.refer_result_id = this.referOut.receive_refer_result_id;
          this.serviceplan_id = this.referOut.receive_serviceplan_id;
          this.receive_spclty_id = this.referOut.receive_spclty_id;
          this.receive_ward_id = this.referOut.receive_ward_id;
          this.refer_reject_reasons = this.referOut.reject_refer_reason;
          this.doctor_license = this.referOut.doctor_id.split(',')[0];
          
          this.diag_text= this.signtext.diag_text;
          this.expire_date =  moment(new Date().getTime() + 365 * 86400E3).tz('Asia/Bangkok').format('YYYY-MM-DD')
          
          this.textCc = this.signtext.cc;
          this.textPmh = this.signtext.pmh;
          if(this.pe){
            this.textPe = this.signtext.pe; 
          }
          if(this.hpi){
            this.textHpi = this.signtext.hpi;
          }
  
          if (this.signtext.body_weight_kg && this.signtext.height_cm){
            this.bmi = (this.signtext.body_weight_kg / ((this.signtext.height_cm / 100) * (this.signtext.height_cm / 100)));
          }
  
  
          if (this.profile.chwpart && this.profile.amppart && this.profile.tmbpart) {
            if(this.profile.moopart  && this.profile.moopart !="" && this.profile.moopart !="00"){
              this.getAddress_full(this.profile.chwpart,this.profile.amppart,this.profile.tmbpart,this.profile.moopart);
            } else{
              this.getAddress(this.profile.chwpart,this.profile.amppart,this.profile.tmbpart);
            }
        }

          
        } else {
  
  
        }
        this.blockedDocument = false;
        
    } catch (error) {
        console.log(error);
        this.blockedDocument = false;

    }
}
lookupDatas(){
  this.getTypept();
  this.getStrength();
  this.getLoads();
  this.getResult();
  this.getServiceplan();
  this.getStation();
  this.getService();
}

async getTypept(){
  try {
    let rs:any = await this.ketTypeptService.select();
    this.itemeTypept = rs;
  } catch (error) {
    console.log('error',error);
  }
}

async getStrength(){
  try {
    let rs:any = await this.ketStrengthService.select();
    this.itemeStrength = rs;
  } catch (error) {
    console.log('error',error);
  }
}

async getResult(){
  try {
    let rs:any = await this.ketReferResultService.select();
    this.result = rs;

  } catch (error) {
    console.log('error',error);
    
  }
}

async getServiceplan(){
  try {
    let rs:any = await this.ketServiceplanService.select();
    this.serviceplan = rs;
  } catch (error) {
    console.log('error',error);
    
  }
}

async getStation(){
  try {
    let rs:any = await this.ketStationService.select();
    this.station = rs;
  } catch (error) {
    console.log('error',error);
    
  }
}

async getService(){
  try {
    let rs:any = await this.servicesService.department();
    this.department = rs;
  } catch (error) {
    console.log('error',error);
    
  }
}

async getLoads(){
  try {
    let rs:any = await this.ketLoadsService.select();
    this.itemeLoads = rs; 
  } catch (error) {
    console.log('error',error);
  }
}

async getAddress_full(chwpart:any,amppart:any,tmbpart:any,moopart:any){
  try {
    let rs:any = await this.ketThaiaddressService.select_full(chwpart,amppart,tmbpart,moopart);
    this.textAddress = rs[0].full_name
  } catch (error) {
    console.log('error',error);
  }
}

async getAddress(chwpart:any,amppart:any,tmbpart:any){
  try {
    let rs:any = await this.ketThaiaddressService.select(chwpart,amppart,tmbpart);
    this.textAddress = rs[0].full_name
  } catch (error) {
    console.log('error',error);
  }
}

}
