import { Component } from '@angular/core';
import { AppMainComponent } from './app.main.component';
import { VersionService } from 'src/app/services-api/version.service'

@Component({
    selector: 'app-footer',
    templateUrl: './app.footer.component.html',
    styles: ['.footer-app {position: fixed;background-color: #7f8fa6; bottom: 0px; width:100%; height: 40px !important; z-index: 102; }']
})
export class AppFooterComponent{
    version:any = 'v6.6.1.20241126';
    versionText:any
    version_web:any;
    detail:any;
    constructor(public appMain: AppMainComponent,private versionService: VersionService) {}

    ngOnInit() {
      this.getInfo();
      this.referServer();
    }

    async getInfo() {
        try {
          let rs: any = await this.versionService.list();
          if (rs[0]) {
            let rs_x: any = await this.versionService.insert(this.version);
            // console.log(rs_x);
            this.version_web = rs[0].version_web;
            this.detail = rs[0].detail;

            if(this.version_web>this.version){
                this.versionText = 'กรุณอัพเดทเวอร์ชั่น';
            }else{
                this.versionText = null;
            }
          } 
        } catch (error) {
          console.log('versionService ',error);
          // this.alertService.error();
        }
      }
      get currentYear(): number
      {
          return new Date().getFullYear();
      }
      async referServer() {
        try {
          let rs: any = await this.versionService.referServer();
          let apiUrlSer = rs[0].api_url;
          // console.log("apiUrlSer",apiUrlSer);
          sessionStorage.setItem('apiUrlSer', apiUrlSer);
    
        } catch (error) {
          console.log('apiUrlSer',error);
    
        }
      }
}
