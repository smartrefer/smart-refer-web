import * as moment from 'moment-timezone';
import { Component, OnInit,Input,NgZone, ViewChild} from '@angular/core';
import { Router,NavigationExtras } from '@angular/router';
import {slideInOutAnimation,fadeInAnimation} from 'src/app/animations/index';
import { FormsModule } from '@angular/forms';
import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { KetAppointSlotService } from '../../../services-api/ket-appoint-slot.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { AlertService } from '../../../service/alert.service';
import { Dialog } from 'primeng/dialog';
import { ServicesService } from '../../../services-api/services.service';
import {
    ConfirmationService,
    MessageService,
    ConfirmEventType,
} from 'primeng/api';
import { Message } from 'primeng/api';


interface Days {
  name: string,
  code: string
}

@Component({
  selector: 'app-appoint-slots',
  templateUrl: './appoint-slots.component.html',
  styleUrls: ['./appoint-slots.component.css'],
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }
})
export class AppointSlotsComponent implements OnInit {
addHoliday() {
throw new Error('Method not implemented.');
}

  @ViewChild('dt') table!: Table;

  @ViewChild('dtCreate') tableCreate!: Table;

  itemeStatus: any =[
    { status_id: '1', status_name: 'เปิดบริการ' },
    { status_id: '0', status_name: 'ปิดบริการ' },
  ]
  status_id:any =  '1';

  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;

  blockedPanel: boolean = false;
  info: any;

  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';
  selectedRowsData:any = [];

  hcode: any;
  clinic_code: any;
  clinic_name: any;
  clinic_contact: any;
  max_patient: number;
  day_open: any;
  time_open: any;
  doctor_name: any;
  description: any;
  remark: any;
  is_active: any;

  dayofweek!: Days[];
  selectedDayOfweek!: Days[];

  displayDetail:boolean=false;
  is_selected: boolean = false;
  msgs: Message[] = [];
  sdate: Date;
  edate: Date;
  visible: boolean = false;
  validateForm: boolean = false;
  showDialog() {
      this.visible = true;
  }
  date: Date | undefined;

  minDate: Date | undefined;

  maxDate: Date | undefined;
    content: any;

  constructor(

    private primengConfig: PrimeNGConfig,
    private KetAppointSlotService: KetAppointSlotService,
    private globalVariablesService: GlobalVariablesService,
    private router: Router,
    private alertService: AlertService,
     private confirmationService: ConfirmationService,
  ) {

    this.hcode = sessionStorage.getItem('hcode');
    this.dayofweek = [
      {name: 'จันทร์', code: '1'},
      {name: 'อังคาร', code: '2'},
      {name: 'พุธ', code: '3'},
      {name: 'พฤหัสบดี', code: '4'},
      {name: 'ศุกร์', code: '5'},
      {name: 'เสาร์', code: '6'},
      {name: 'อาทิตย์', code: '7'}
    ];

  }
  readGlobalValue() {
    this.scrHeight = Number(this.globalVariablesService.scrHeight);
    this.scrWidth = Number(this.globalVariablesService.scrWidth);
    this.boxHeight = ((this.scrHeight) - 80) + 'px';

    this.scrollHeight = (Number(this.globalVariablesService.scrHeight) - 360) + 'px';

    this.ptableStyle = {
      width: (this.scrWidth-100) + 'px',
      
      height: (this.scrHeight - 300) + 'px'
    }
    // do something with the value read out
  }

  ngOnInit() {

    this.onLoads();
    this.primengConfig.ripple = true;

  }

  async onLoads(){
    this.loadingCreate = true;

    try {
      let rs: any = await this.KetAppointSlotService.appointslots(
        this.hcode,
      );

      let item: any = rs;
      
      if (item[0]) {
          for(let i of item){
            let d:any = JSON.parse(i.day_open);
            i.day_open_name = '';

            if(d.length > 0){ 
              for(let j = 0 ; j < d.length; j++){ 
              i.day_open_name += this.dayofweek.find((dn:any) => d[j] == dn.code).name + ' ';
            }

            let statusName = this.itemeStatus.find((e: any) => e.status_id == i.is_active);
            i.is_active_name = statusName.status_name;

          }
        }
        
          this.rowsDataCreate = item;
          this.loadingCreate = false;
          // console.log( this.rowsDataCreate );
          
         
      } else {

          this.loadingCreate = false;
      }
    
  } catch (error) {
      console.log(error);
      this.loadingCreate = false;
  }
  }

  async onChangeSave(){

    if(!this.clinic_code){
      this.alertService.info('กรุณาระบุ! Clinic Code')
    }else if(!this.clinic_name){
      this.alertService.info('กรุณาระบุ! Clinic Name')
    }else if(!this.clinic_contact){
      this.alertService.info('กรุณาระบุ! Clinic Contact')
    }else if(!this.max_patient){
      this.alertService.info('กรุณาระบุ! จำนวนที่เปิดรับนัด')
    }else if(!this.selectedDayOfweek){
      this.alertService.info('กรุณาระบุ! วันที่เปิดคลินิก')
    }else if(!this.time_open){
      this.alertService.info('กรุณาระบุ! ช่วงเวลาที่เปิดคลินิก')
    }else if(!this.doctor_name){
      this.alertService.info('กรุณาระบุ! แพทย์ผู้ตรวจ')
    }else{
      this.info = {
          "hcode": this.hcode,
          "clinic_code": this.clinic_code,
          "clinic_name": this.clinic_name,
          "clinic_contact": this.clinic_contact,
          "max_patient": +this.max_patient,
          "day_open": JSON.stringify(this.selectedDayOfweek),
          "time_open": this.time_open,
          "doctor_name": this.doctor_name,
          "description": this.description,
          "remark": this.remark,
          "is_active": this.status_id
      }
      try {
        let data: any = {};
        data.rows = this.info;

        
        if(!this.is_selected){
          /* บันทึก ข้อมูล */
          let rs:any = await this.KetAppointSlotService.onSave(data);

          if(rs.length > 0){
            this.blockedPanel = false;
            this.alertService.successPro('บันทึกสำเร็จเรียบร้อย','ดำเนินการเสร็จเรียบร้อย');
            this.onLoads();
            this.onChangeClear();
          }else{
            this.blockedPanel = false;
            this.alertService.error(JSON.stringify(rs.res_.error),'เกิดข้อผิดพลาด! บันทึกข้อมูลไม่สำเร็จ');
          }
        }else{
          /* บันทึก แก้ไขข้อมูล */
          let rs:any = await this.KetAppointSlotService.onUpdate(this.info,this.hcode,this.clinic_code);

          if(rs.length > 0){
            this.blockedPanel = false;
            this.alertService.successPro('บันทึกสำเร็จเรียบร้อย','ดำเนินการเสร็จเรียบร้อย');
            this.onLoads();
            this.onChangeClear();

          }else{
            this.blockedPanel = false;
            this.alertService.error(JSON.stringify(rs.res_.error),'เกิดข้อผิดพลาด! บันทึกข้อมูลไม่สำเร็จ');
          }
        }  
        
        
      } catch (error) {
        this.blockedPanel = false;
        this.alertService.error(JSON.stringify(error),'ข้อมูลผิดพลาด');  
      }
  }
  }

  async onSelect(item:any) {
    this.is_selected = true;
    this.hcode = item.hcode;
    this.clinic_code = item.clinic_code;
    this.clinic_name = item.clinic_name;
    this.clinic_contact = item.clinic_contact;
    this.max_patient = item.max_patient;
    this.selectedDayOfweek = JSON.parse(item.day_open);
    this.time_open = item.time_open;
    this.doctor_name = item.doctor_name;
    this.description = item.description;
    this.remark = item.remark;
    this.status_id = item.is_active;

    //console.log("Click");
  }
  async onChangeClear(){
    this.is_selected = false;
    this.hcode = '';
    this.clinic_code = '';
    this.clinic_name = '';
    this.clinic_contact = '';
    this.max_patient = null;
    this.selectedDayOfweek = null;
    this.time_open = '';
    this.doctor_name = '';
    this.description = '';
    this.remark = '';
    this.status_id = '1';
  }

   async showDetail(data: any) {
          let ptName = data.fullname;
  console.log("Click show");
          try {
  
              // let rs: any = await this.servicesService.patient(data.cid);
  
              // this.textHN = rs.data.hn;
              // this.content =
              //     '  ชื่อ-สกุล : ' + ptName + '<br>' + 'HN : ' + this.textHN;
  
              // this.displayDetail = true;
              this.content ='  วันที่ : ';

              this.confirmationService.confirm({
                  message: this.content,
                  header: 'เพิ่มวันหยุดคลินิค',
                  icon: 'pi pi-plus',
                  accept: () => {
                      this.msgs = [{severity:'info', summary:'Confirmed', detail:'You have accepted'}];
                    //  this.copyText();
                  },
                  reject: (type) => {
                      switch (type) {
                          case ConfirmEventType.REJECT:
                              this.msgs = [
                                  {
                                      severity: 'error',
                                      summary: 'Rejected',
                                      detail: 'You have rejected',
                                  },
                              ];
                              break;
                          case ConfirmEventType.CANCEL:
                              this.msgs = [
                                  {
                                      severity: 'warn',
                                      summary: 'Cancelled',
                                      detail: 'You have cancelled',
                                  },
                              ];
                              break;
                      }
                  },
              });
          } catch (error) {
              console.log(error);
           //   this.displayDetail = true;
  
              this.confirmationService.confirm({
                  message: 'เกิดข้อผิดพลาด',
                  header: 'ข้อมูล',
                  icon: 'pi pi-exclamation-triangle',
                  accept: () => {
                      this.msgs = [
                          {
                              severity: 'info',
                              summary: 'Confirmed',
                              detail: 'You have accepted',
                          },
                      ];
  
                  },
                  reject: (type) => {
                      switch (type) {
                          case ConfirmEventType.REJECT:
                              this.msgs = [
                                  {
                                      severity: 'error',
                                      summary: 'Rejected',
                                      detail: 'You have rejected',
                                  },
                              ];
                              break;
                          case ConfirmEventType.CANCEL:
                              this.msgs = [
                                  {
                                      severity: 'warn',
                                      summary: 'Cancelled',
                                      detail: 'You have cancelled',
                                  },
                              ];
                              break;
                      }
                  },
              });
          }
      }
      onDateSelected() {
        // console.log('lddl');
        if (this.edate < this.sdate) {
            this.alertService.error(
                'ข้อผิดพลาด !',
                'วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้'
            );
            this.validateForm = false;
        } else {
            this.validateForm = true;
        }
    }

}
