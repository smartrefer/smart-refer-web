import * as moment from 'moment-timezone';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ThisReceiver } from '@angular/compiler';

//lookup 
import { ServicesService } from '../../../services-api/services.service';
import { KetTypeptService } from '../../../services-api/ket-typept.service';
import { KetStrengthService } from '../../../services-api/ket-strength.service';
import { KetLoadsService } from '../../../services-api/ket-loads.service';
import { KetThaiaddressService } from '../../../services-api/ket-thaiaddress.service';
import { KetReferoutService } from '../../../services-api/ket-referout.service';
import { KetAttachmentService } from '../../../services-api/ket-attachment.service';
import { KetReferResultService } from '../../../services-api/ket-refer-result.service';
import { KetServiceplanService } from '../../../services-api/ket-serviceplan.service';
import { KetStationService } from '../../../services-api/ket-station.service';
import { async } from '@angular/core/testing';
import { KetAppointSlotService } from '../../../services-api/ket-appoint-slot.service';

@Component({
  selector: 'app-printappoint-auto',
  templateUrl: './printappoint-auto.component.html',
  styleUrls: ['./printappoint-auto.component.css']
})
export class PrintappointAutoComponent implements OnInit {
 

  blockedDocument: boolean = false;
  referNo: any;
  datas:any;
  currentDate: Date = new Date();
  constructor(
    private primengConfig: PrimeNGConfig,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private ketTypeptService: KetTypeptService,
    private ketStrengthService: KetStrengthService,
    private ketLoadsService: KetLoadsService,
    private ketThaiaddressService: KetThaiaddressService,
    private ketReferoutService: KetReferoutService,
    private ketAttachmentService: KetAttachmentService,
    private ketReferResultService: KetReferResultService,
    private ketServiceplanService: KetServiceplanService,
    private ketStationService: KetStationService,
    private ketAppointSlotService: KetAppointSlotService,
  ) { }

  ngOnInit(): void {
  

    const url = this.router.url;
    this.referNo = url.split('/').pop();
    if (this.referNo) {
      this.getAppointAuto('10954-1-68001185');
    }

  }

  print() {
    let printContents = document.getElementById("print-section").innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.location.reload();
  }

  
  async getAppointAuto(refer_no: any) {
    console.log('getAppointAuto',refer_no);
    this.blockedDocument = true;
    try {
      let rs: any = await this.ketReferoutService.getAppointAuto(refer_no,);

      console.log(rs);
      this.datas = rs[0];
       
      this.blockedDocument = false;

    } catch (error) {
      console.log(error);
      this.blockedDocument = false;

    }
  }

  


}
