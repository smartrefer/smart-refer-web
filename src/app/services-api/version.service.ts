import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VersionService {
  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async list() {
    const _url = `${this.apiUrl}/version/select`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async insert(v:any) {
    const _url = `${this.apiUrl}/version/insert/${v}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async referServer(){
    let hcode = sessionStorage.getItem('hcode');
    const _url = `${this.apiUrl}/referserver/select/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async referSerferPhr(){
    const _url = `${this.apiUrl}/referserver/selectPhr`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async selectReferboard(){
    const _url = `${this.apiUrl}/referserver/selectReferboard`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

 async selectServer() {
  const _url = `${this.apiUrl}/referserver/selectServer`;
  return this.httpClient.get(_url,this.httpOptions).toPromise();
 }
}