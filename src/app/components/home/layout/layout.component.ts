import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  title = 'smartrefer';
  cars=[{year:2564,brand:'11/11/2564'}];
  items= [{label: 'Smartrefer', icon: 'pi pi-fw pi-bars'},
  {label: 'Refer Out', icon: 'fas fas-fw fa-shipping-fast'},
  {label: 'Refer In', icon: 'fas fas-fw fa-wheelchair'},
  {label: 'Refer Back', icon: 'fas fas-fw fa-ambulance'},
  {label: 'Refer Receive', icon: 'fas fas-fw fa-heart'},
  {label: 'Appoint', icon: 'far fas-fw fa-calendar-alt'},
  {label: 'Report', icon: 'far fas-fw fa-clipboard'},
  {label: 'Evaluate', icon: 'fas fas-fw fa-tasks'}];


  constructor() { }

  ngOnInit(): void {
  }

}
