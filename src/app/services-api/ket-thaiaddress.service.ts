import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class KetThaiaddressService {
  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async select(chwpart:any,amppart:any,tmbpart:any) {
    const _url = `${this.apiUrl}/address/select/${chwpart}/${amppart}/${tmbpart}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async select_full(chwpart:any,amppart:any,tmbpart:any,moopart:any) {
    const _url = `${this.apiUrl}/address/select_full/${chwpart}/${amppart}/${tmbpart}/${moopart}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

}
